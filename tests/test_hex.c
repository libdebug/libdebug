
/*
 * Copyright (c) 2002-2004  Abraham vd Merwe <abz@blio.com>
 * Copyright (c) 2016  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *	  notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *	  notice, this list of conditions and the following disclaimer in the
 *	  documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the author nor the names of other contributors
 *	  may be used to endorse or promote products derived from this software
 *	  without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/fcntl.h>
#include <unistd.h>

#include <debug/memory.h>
#include <debug/log.h>
#include <debug/hex.h>

#define SIZE 8192

static void *getfile (int fd,size_t *length)
{
   size_t size = SIZE;
   char *buf;
   ssize_t result;

   if ((buf = mem_alloc (size)) == NULL)
	 {
		log_printf (LOG_ERROR,"failed to allocate memory: %m\n");
		return (NULL);
	 }

   *length = 0;

   while ((result = read (fd,buf + *length,size - *length)) > 0)
	 {
		*length += result;

		if (*length == size)
		  {
			 void *ptr;

			 size += SIZE;

			 if ((ptr = mem_realloc (buf,size)) == NULL)
			   {
				  log_printf (LOG_ERROR,"failed to allocate memory: %m\n");
				  mem_free (buf);
				  return (NULL);
			   }

			 buf = ptr;
		  }
	 }

   if (result < 0)
	 {
		log_printf (LOG_ERROR,"read: %m\n");
		mem_free (buf);
		return (NULL);
	 }

   return (buf);
}

int main (int argc,char *argv[])
{
   void *buf;
   size_t length;
   int fd;

   mem_open (NULL);
   log_open (NULL,LOG_NORMAL,0);
   atexit (mem_close);
   atexit (log_close);

   if (argc != 2)
	 {
		const char *progname;

		(progname = strrchr (argv[0],'/')) != NULL ? progname++ : (progname = argv[0]);
		log_printf (LOG_ERROR,"usage: %s <filename>\n",progname);
		exit (EXIT_FAILURE);
	 }

   if (strcmp(argv[1], "-") == 0)
	 fd = 0;
   else if ((fd = open (argv[1],O_RDONLY)) < 0)
	 {
		log_printf (LOG_ERROR,"open %s: %m\n",argv[1]);
		exit (EXIT_FAILURE);
	 }

   if ((buf = getfile (fd,&length)) == NULL)
	 {
		close (fd);
		exit (EXIT_FAILURE);
	 }

   hexdump (LOG_NORMAL,buf,length);
   log_flush ();

   mem_free (buf);
   close (fd);

   exit (EXIT_SUCCESS);
}

