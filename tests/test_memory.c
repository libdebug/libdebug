
/*
 * Copyright (c) 2002-2004  Abraham vd Merwe <abz@blio.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *	  notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *	  notice, this list of conditions and the following disclaimer in the
 *	  documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the author nor the names of other contributors
 *	  may be used to endorse or promote products derived from this software
 *	  without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <string.h>

#include <debug/memory.h>
#include <debug/log.h>
#include <debug/hex.h>

#define TEST_ALLOC
#define TEST_REALLOC

#ifdef TEST_ALLOC
static void *xalloc (size_t count)
{
   void *tmp;

   if ((tmp = mem_alloc (count)) == NULL)
	 {
		log_printf (LOG_ERROR,"alloc failed: %m\n");
		exit (EXIT_FAILURE);
	 }

   return (tmp);
}
#endif	/* #ifdef TEST_ALLOC */

#ifdef TEST_REALLOC
static void *xrealloc (void *ptr,size_t count)
{
   void *tmp;

   if ((tmp = mem_realloc (ptr,count)) == NULL)
	 {
		log_printf (LOG_ERROR,"realloc failed: %m\n");
		mem_free (ptr);
		exit (EXIT_FAILURE);
	 }

   return (tmp);
}
#endif	/* #ifdef TEST_REALLOC */

#ifdef TEST_ALLOC
static void test_alloc (void)
{
   void *tmp;
   int i;

   for (i = 1; i < 1024*1024; i += 1025)
	 {
		tmp = xalloc (i);
		memset (tmp,0x55,i);
		memset (tmp,0xaa,i);
		mem_free (tmp);
	 }
}
#endif	/* #ifdef TEST_ALLOC */

#ifdef TEST_REALLOC
static void test_realloc (void)
{
   unsigned char *tmp = NULL,pattern = 0x55;
   int i,j,k;

   for (i = 0; i < 16; i++)
	 {
		tmp = xrealloc (tmp,(i + 1) * 1025);
		memset (tmp + i * 1025,pattern,1025);

		for (j = 0; j < i + 1; j++)
		  for (k = 0; k < 1025; k++) if (tmp[k + j * 1025] != pattern)
			{
			   mem_free (tmp);
			   log_printf (LOG_ERROR,"realloc test failed on round %d at position %d\n",i,j);
			   exit (EXIT_FAILURE);
			}

		log_printf (LOG_NORMAL,"round %d succeeded\n",i);
	 }

   mem_free (tmp);
}
#endif	/* #ifdef TEST_REALLOC */

int main (void)
{
   mem_open (NULL);
   log_open (NULL,LOG_NOISY,LOG_HAVE_COLORS | LOG_PRINT_FUNCTION);
   atexit (mem_close);
   atexit (log_close);

#ifdef TEST_ALLOC
   test_alloc ();
#endif	/* #ifdef TEST_ALLOC */

#ifdef TEST_REALLOC
   test_realloc ();
#endif	/* #ifdef TEST_REALLOC */

   exit (EXIT_SUCCESS);
}

